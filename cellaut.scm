;; Rule 90 &c.

;; Copyright © 2022 stdh (https://codeberg.org/stdh/); and also
;; Copyleft 🄯 2022 stdh (https://codeberg.org/stdh/).
;; Licensed under GNU Affero General Public License, version 3,
;; see LICENSE (or https://www.gnu.org/licenses/agpl-3.0.html)
;; No warranties!

(use-modules (srfi srfi-1))
(use-modules (ice-9 format))

;; Binary '1' as yes, '0' as no:
;(define yes #\*)
;(define yes #\λ)
(define yes #\■)
(define no #\sp)
;; Beyond the visible cells lies:
(define border no)

;; Rows of yes/no cells:
(define (nos-with-central-yes count)
  (let ((leading-nos (floor (/ count 2))))
    (append (make-list leading-nos no)
            (list yes)
            (make-list (- count leading-nos 1) no))))

(define (random-yns count)
  (list-tabulate count (lambda (_)
                         (if (= 0 (random 2))
                             yes no))))

(define (print-ynlist ynlist)
  (display (list->string ynlist))
  (newline))

;; Rules take three arguments and return yes/no. See
;; https://en.wikipedia.org/wiki/Elementary_cellular_automaton\#The_numbering_system
(define (make-rule wolfram-code)             ;LCR
  (let* ((all-input (list (list yes yes yes) ;111
                          (list yes yes no ) ;110
                          (list yes no  yes) ;etc.
                          (list yes no  no )
                          (list no  yes yes)
                          (list no  yes no )
                          (list no  no  yes)
                          (list no  no  no )))
         (code-bits (string->list
                     (format #f "~8,'0b" wolfram-code)))
         ;; These code-bits indicate which inputs lead to 'yes:
         (positive-input
          (filter-map (lambda (input bit)
                        (if (equal? bit #\1) input #f))
                      all-input code-bits)))
    (lambda (left center right)
      (if (member (list left center right)
                  positive-input)
          yes
          no))))

;; Apply rule to row of yes/no cells:
(define (rule-apply rule ynlist)
  (map rule
       (cons border ynlist) ;; -> Longer than ynlist,
       ynlist               ;;    but srfi-1's map can handle it.
       (append (cdr ynlist) (list border))))

;; Combined:
(define* (show-rule
          #:optional
          (rule (make-rule 90)) ;classic
          (count 40)
          (ynlist (nos-with-central-yes 72))) ;fit users' punched card
  (do ((i 1 (1+ i))
       (yns ynlist (rule-apply rule yns)))
      ((> i count))
    (print-ynlist yns)))

